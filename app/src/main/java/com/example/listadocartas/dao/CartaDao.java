package com.example.listadocartas.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;


import com.example.listadocartas.pojo.Carta;

import java.util.List;

@Dao
public interface CartaDao {

    @Query("SELECT * FROM carta WHERE name like '%' || :name || '%' ")
    LiveData<List<Carta>> getCartas(String name);

    @Insert
    void addCarta(Carta carta);

    @Insert
    void addCartas(List<Carta> cartas);

    @Delete
    void deletecarta(Carta carta);

    @Query("DELETE FROM Carta")
    void deleteCards();

}
