package com.example.listadocartas;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.listadocartas.databinding.LvCartesRowBinding;
import com.example.listadocartas.pojo.Carta;

import java.util.List;

public class CardsAdapter extends ArrayAdapter<Carta> {


    public CardsAdapter(@NonNull Context context, int resource, @NonNull List<Carta> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Carta carta = getItem(position);
        Log.w("CARTA", carta.toString());

        LvCartesRowBinding binding = null;

        if (convertView == null) {
            binding = LvCartesRowBinding.inflate(
                    LayoutInflater.from(getContext()),
                    parent,
                    false
            );

        }else {
            binding = LvCartesRowBinding.bind(convertView);
        }


        String name = carta.getNumber() + " - " + carta.getName();
        binding.cartaName.setText(name);
        binding.cartaType.setText(carta.getType());

        Glide.with(getContext()).load(
                carta.getImageUrl()
        ).into(binding.ivCartaImage);

        return binding.getRoot();
    }
}
