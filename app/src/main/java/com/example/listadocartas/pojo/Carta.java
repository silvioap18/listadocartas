package com.example.listadocartas.pojo;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Carta implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String manaCost;
    private double cmc;
    private String colors;
    private String type;
    private String rarity;
    private String number;
    private String imageUrl;

    @Override
    public String toString() {
        return "Carta{" +
                "name='" + name + '\'' +
                ", manaCost='" + manaCost + '\'' +
                ", cmc=" + cmc +
                ", colors='" + colors + '\'' +
                ", type='" + type + '\'' +
                ", rarity='" + rarity + '\'' +
                ", number=" + number +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
