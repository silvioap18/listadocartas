package com.example.listadocartas.ui.main;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.listadocartas.AppDatabase;
import com.example.listadocartas.CartasDBAPI;
import com.example.listadocartas.dao.CartaDao;
import com.example.listadocartas.pojo.Carta;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainViewModel extends AndroidViewModel {

    private final Application app;
    private final AppDatabase appDatabase;
    private final CartaDao cartaDao;
    private LiveData<List<Carta>> cartas;

    public MainViewModel(Application application) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(
                this.getApplication());
        this.cartaDao = appDatabase.getCartaDao();
    }

    public LiveData<List<Carta>> getCartas(String name) {
        return cartaDao.getCartas(name);
    }

    public void reload() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        executor.execute(() -> {
            CartasDBAPI api = new CartasDBAPI();
            ArrayList<Carta> cartas = api.getCards();
            cartaDao.deleteCards();
            cartaDao.addCartas(cartas);

        });


    }

    // TODO: Implement the ViewModel
}