package com.example.listadocartas.ui.main;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.listadocartas.CardsAdapter;
import com.example.listadocartas.CartasDBAPI;
import com.example.listadocartas.DetailActivity;
import com.example.listadocartas.R;
import com.example.listadocartas.SharedViewModel;
import com.example.listadocartas.databinding.MainFragmentBinding;
import com.example.listadocartas.pojo.Carta;

import java.util.ArrayList;


public class MainFragment extends Fragment {

    private MainViewModel mViewModel;

    private ArrayList<Carta> items;
    private CardsAdapter adapter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        MainFragmentBinding binding = MainFragmentBinding.inflate(inflater);

        View view = binding.getRoot(); /*inflater.inflate(R.layout.main_fragment, container, false);*/


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String name = preferences.getString("name", "");

        items = new ArrayList<>();

        adapter = new CardsAdapter(
                getContext(),
                R.layout.lv_cartes_row,
                items
        );

        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        mViewModel.getCartas(name).observe(getViewLifecycleOwner(), cartas -> {
            adapter.clear();
            adapter.addAll(cartas);
        });

        SharedViewModel sharedViewModel = new ViewModelProvider(getActivity()).get(SharedViewModel.class);

        binding.lvCartes.setAdapter(adapter);

        binding.lvCartes.setOnItemClickListener((parent, view1, position, id) ->{
            Carta carta = adapter.getItem(position);

            if (!esTablet()){
                Intent intent = new Intent(getContext(), DetailActivity.class);
                intent.putExtra("carta", carta);

                startActivity(intent);
            }else {
                sharedViewModel.select(carta);
            }

        });



        return view;
    }

    void refresh(){
        mViewModel.reload();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cartas, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh){
            mViewModel.reload();
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(getContext(), SettingsActivity.class);
            startActivity(i);
            /*refresh();*/
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Carta>> {

        @Override
        protected ArrayList<Carta> doInBackground(Void... voids) {
            CartasDBAPI api = new CartasDBAPI();
            ArrayList<Carta> cartas = api.getCards();
            Log.d("DEBUG", String.valueOf(cartas));
            return cartas;
        }


        @Override
        protected void onPostExecute(ArrayList<Carta> cartas) {
            adapter.clear();
            for (Carta carta : cartas) {
                adapter.add(carta);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String name = preferences.getString("name", "");

        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        mViewModel.getCartas(name).removeObservers(getViewLifecycleOwner());

        mViewModel.getCartas(name).observe(getViewLifecycleOwner(), cartas -> {
            adapter.clear();
            adapter.addAll(cartas);
        });

    }


    boolean esTablet (){
        return getResources().getBoolean(R.bool.tablet);
    }



}