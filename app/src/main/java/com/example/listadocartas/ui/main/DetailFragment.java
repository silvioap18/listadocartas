package com.example.listadocartas.ui.main;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.listadocartas.R;
import com.example.listadocartas.SharedViewModel;
import com.example.listadocartas.databinding.MainFragment2Binding;
import com.example.listadocartas.pojo.Carta;

public class DetailFragment extends Fragment {

    private DetailViewModel mViewModel;
    private MainFragment2Binding binding;

    public static DetailFragment newInstance() {
        return new DetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = MainFragment2Binding.inflate(inflater);

        View view = binding.getRoot();

        Intent intent = getActivity().getIntent();

        if (intent != null) {
            Carta carta = (Carta) intent.getSerializableExtra("carta");

            if (carta != null) {
                showData(carta);
            }

        }

        SharedViewModel sharedViewModel = new ViewModelProvider(getActivity()).get(SharedViewModel.class);
        sharedViewModel.getSelected().observe(getViewLifecycleOwner(), this::showData);

        return view;
    }


    @SuppressLint("SetTextI18n")
    private void showData(Carta carta) {

        System.out.println(carta.toString());

        String name = carta.getNumber() + " - " + carta.getName();
        System.out.println(name);

        binding.cartaNameAct.setText(name);
        binding.cartaManaCostAct.setText("Mana cost: " + carta.getManaCost());
        binding.cartaCmcAct.setText("CMC: " + String.valueOf(carta.getCmc()));
        binding.cartaTypeAct.setText("Type: " + carta.getType());
        binding.cartaRarityAct.setText("Rarity: " + carta.getRarity());

        Glide.with(getContext()).load(
                carta.getImageUrl()
        ).into(binding.ivCartaImageAct);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        // TODO: Use the ViewModel
    }

}