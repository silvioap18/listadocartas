package com.example.listadocartas;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.listadocartas.dao.CartaDao;
import com.example.listadocartas.pojo.Carta;

@Database(entities = {Carta.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(
                    context.getApplicationContext(),
                    AppDatabase.class, "db"
            ).build();
        }
        return INSTANCE;
    }

    public abstract CartaDao getCartaDao();

}
