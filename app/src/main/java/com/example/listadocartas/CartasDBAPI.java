package com.example.listadocartas;

import android.net.Uri;

import com.example.listadocartas.pojo.Carta;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class CartasDBAPI {
    private final String BASE_URL = "https://api.magicthegathering.io";
    private final String API_KEY = "<api-key>";

    public ArrayList<Carta> getCards() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("v1")
                .appendPath("cards")
                .build();
        String url = builtUri.toString();

        return doCall(url);
    }

    public ArrayList<Carta> getCardsWithSpecificRarity(String rarity) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("v1")
                .appendPath("cards")
                .appendPath("movie")
                .appendQueryParameter("rarity", rarity)
                /*.appendQueryParameter("language", idioma)*/
                .build();
        String url = builtUri.toString();

        return doCall(url);
    }

    private ArrayList<Carta> doCall(String url) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Carta> processJson(String jsonResponse) {
        ArrayList<Carta> cards = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonCards = data.getJSONArray("cards");
            for (int i = 0; i < jsonCards.length(); i++) {
                JSONObject jsonCard = jsonCards.getJSONObject(i);

                Carta carta = new Carta();
                carta.setName(jsonCard.getString("name"));
                carta.setManaCost(jsonCard.getString("manaCost"));
                carta.setCmc(jsonCard.getDouble("cmc"));
                carta.setColors(jsonCard.getJSONArray("colors").getString(0));
                carta.setType(jsonCard.getString("type"));
                carta.setRarity(jsonCard.getString("rarity"));
                carta.setNumber(jsonCard.getString("number"));
                if (jsonCard.has("imageUrl")){
                    carta.setImageUrl(jsonCard.getString("imageUrl"));
                }else
                    carta.setImageUrl(null);

                cards.add(carta);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cards;
    }

}
